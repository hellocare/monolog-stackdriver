# Usage

## General

This package using an environment variable `GOOGLE_CLOUD_CREDENTIALS` (contains a path to json auth file) to identify on Google Cloud Platform. 

## Symfony
In `services.yaml`
```yaml
Hellocare\Monolog\StackDriverHandler:
  arguments:
    $googleProjectId: '<google_project_name>'
```

In `monolog.yaml`
```yaml
monolog:
  handlers:
    stackdriver:
      type: service
      id: Hellocare\Monolog\StackDriverHandler
```