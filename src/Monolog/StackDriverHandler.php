<?php

namespace Hellocare\Monolog;

use Google\Cloud\Logging\Logger;
use Google\Cloud\Logging\LoggingClient;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\AbstractProcessingHandler;

class StackDriverHandler extends AbstractProcessingHandler
{
    /**
     * @var array
     */
    private $options;

    /**
     * @var LoggingClient
     */
    private $client;

    /**
     * @var Logger[]
     */
    private $loggers;

    /**
     * @param string $googleProjectId
     * @param array $options
     */
    public function __construct(string $googleProjectId, $options = [])
    {
        $this->client = new LoggingClient(
            [
                'projectId' => $googleProjectId,
            ]
        );

        $this->options = array_merge(
            [
                'resource' => [
                    'type' => 'global',
                ],
                'labels' => [
                    'project_id' => $googleProjectId,
                    'hostname' => gethostname(),
                    'php_version' => phpversion(),
                ],
                'timestamp' => (new \DateTime())->format(\DateTime::RFC3339),
            ],
            $options
        );

        parent::__construct();
    }

    public function write(array $record): void
    {

        $this->getLogger($record['channel'])->write($record, array_merge($this->options, ['severity' => $record['level_name']]));
    }

    private function getLogger(string $channel): Logger
    {
        if (!isset($this->loggers[$channel])) {
            $this->loggers[$channel] = $this->client->logger($channel, ['labels' => ['context' => $channel]]);
        }

        return $this->loggers[$channel];

    }
}
